package main

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"os"
	"time"

	"github.com/google/uuid"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/libraries/messaging/common"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/pre-authorization-bridge/pkg/messaging"
)

func main() {
	client, _ := cloudeventprovider.New(
		cloudeventprovider.Config{Protocol: cloudeventprovider.ProtocolTypeNats, Settings: cloudeventprovider.NatsConfig{
			Url:          "nats://localhost:4222",
			TimeoutInSec: time.Minute,
		}},
		cloudeventprovider.ConnectionTypeReq,
		messaging.TopicGenerateAuthorization,
	)
	reader := bufio.NewReader(os.Stdin)
	for {
		var req = messaging.GenerateAuthorizationReq{
			Request: common.Request{
				TenantId:  "tenant_space",
				RequestId: uuid.NewString(),
			},
			TwoFactor: messaging.TwoFactor{
				Enabled: false,
			},
			CredentialConfigurationId: "DeveloperCredential",
			CredentialIdentifier:      []string{"my-identifier"},
		}

		b, _ := json.Marshal(req)

		testEvent, _ := cloudeventprovider.NewEvent("test-issuer", messaging.EventTypeGenerateAuthorization, b)

		ev, _ := client.RequestCtx(context.Background(), testEvent)

		var rep messaging.GenerateAuthorizationRep

		json.Unmarshal(ev.Data(), &rep)

		fmt.Println(rep)
		reader.ReadString('\n')
	}
}
