package database

import (
	"context"
	"fmt"
	"time"

	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/pre-authorization-bridge/pkg/messaging"
)

var ErrKeyNotFound = fmt.Errorf("key not in database")

type Database interface {
	SaveAuth(ctx context.Context, key string, authentication messaging.Authentication, ttl time.Duration) error
	GetAuth(ctx context.Context, key string) (*messaging.Authentication, error)
	DeleteAuth(ctx context.Context, key string) (bool, error)
}
